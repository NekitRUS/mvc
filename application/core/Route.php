<?php
class Route {
    static function Start(){
        $controller_name = 'main';
        $action_name = 'index';
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        
        if(!empty($routes[2])){
            $controller_name = $routes[2];
        }
        
        if(!empty($routes[3])){
            $action_name = $routes[3];
        }
        
        $model_name = 'Model_' . $controller_name;
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'Action_' . $action_name;
        
        $model_path = "application/models/" . strtolower($model_name) . '.php';
        if(file_exists($model_path)){
            include $model_path;
        }
        
        $controller_path = "application/controllers/" . strtolower($controller_name) . '.php';
        if(file_exists($controller_path)){
            include $controller_path;
        }
        else{
            Route::ErrorPage404();
        }     
        
        $controller = new $controller_name;
        if(method_exists($controller, $action_name)){
            $controller->$action_name();
        }                
        else {
            Route::ErrorPage404();
        }
    }
    
    function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/MVC/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}
