<?php
Class Controller_guestBook extends Controller{
    function __construct() {
        $this->model = new Model_guestBook();
        $this->view = new View();
    }
    
    function action_index() {
    
        $userIsEmpty = false;					
        $emailIsEmpty = false;				
        $msgIsEmpty = false;
        $msgTooLong = false;
        $error = array();
        $usersData = array();
        $post = $_SERVER["REQUEST_METHOD"] == "POST";

        if ($post) {
            $usersData = ['user' => trim($_POST['user']), 'email' => trim($_POST['email']), 'msg' => trim($_POST['message'])];

            if ($usersData["user"] == "") {
                $userIsEmpty = true;
                $error['user'] = true;
            }
            if ($usersData["email"] == "") {
                $emailIsEmpty = true;
                $error['email'] = true;
            }
            if ($usersData["msg"] == "") {
                $msgIsEmpty = true;
                $error['msgEmpty'] = true;
            }
            if (strlen($usersData["msg"]) > 1000) {
                $msgTooLong = true;
                $error['msgLong'] = true;
            }

            if (!$userIsEmpty && !$emailIsEmpty && !$msgIsEmpty && !$msgTooLong) {
                $this->model->insert_data(['Name' => $usersData["user"], 'Email' => $usersData["email"], 'Msg' => $usersData["msg"]]);
                $usersData = array();
            }
        }
        
        $data = $this->model->get_data();
        $data['error'] = $error;
        $data['info'] = $usersData;
        $this->view->generate('guestbook_view.php', 'template_view.php', $data);
    }
}