<?php
Class Controller_auth extends Controller{
    function __construct() {
        $this->model = new Model_auth();
        $this->view = new View();
    }
    
    function action_index() {
        $logLoginIsEmpty = false;					
        $logPasswordIsEmpty = false;
        $logUser = false;
        $regLoginIsEmpty = false;
        $regPasswordIsEmpty = false;
        $regNameIsEmpty = false;
        $regUser = false;
        $error = array();
        $usersData = array();
        $post = $_SERVER["REQUEST_METHOD"] == "POST";
        $login = isset($_POST['log']);
        $registration = isset($_POST['reg']);
        
        session_start();
        session_destroy();

        if ($post && $login) {
            $usersData = ['login' => trim($_POST['logLogin']), 'password' => trim($_POST['logPassword'])];

            if ($usersData["login"] == "") {
                $logLoginIsEmpty = true;
            }
            if ($usersData["password"] == "") {
                $logPasswordIsEmpty = true;
            }

            if (!$logLoginIsEmpty && !$logPasswordIsEmpty) {
                $logUser = $this->model->get_user($usersData);
                if($logUser){
                    session_start();
                    $_SESSION['full_name'] = $logUser['full_name'];
                    header('Location: main');
                }
                else{
                    $error['mismatch'] = true;
                }
            }
        }
        
        if ($post && $registration) {
            $usersData = ['regLogin' => trim($_POST['regLogin']), 'regPassword' => trim($_POST['regPassword']), 'name' => trim($_POST['name'])];

            if ($usersData["regLogin"] == "") {
                $regLoginIsEmpty = true;
                $error['login'] = true;
            }
            if ($usersData["regPassword"] == "") {
                $regPasswordIsEmpty = true;
                $error['password'] = true;
            }
            if ($usersData["name"] == "") {
                $regNameIsEmpty = true;
                $error['name'] = true;
            }
            
            if (!$regLoginIsEmpty && !$regPasswordIsEmpty && !$regNameIsEmpty){
                $regUser = $this->model->get_verify(['login' => $usersData['regLogin']]);
                if($regUser){
                    $error['alreadyExists'] = true;
                }
                else{
                    $this->model->insert_data([ 'login' => $usersData['regLogin'], 'password' => $usersData['regPassword'], 'name' => $usersData['name'] ]);
                    session_start();
                    $_SESSION['full_name'] = $usersData['name'];
                    header('Location: main');
                }
            }
        }
        
        $data['error'] = $error;
        $data['info'] = $usersData;
        $this->view->generate('auth_view.php', 'template_view.php', $data);
    }
}