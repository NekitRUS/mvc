<?php
class Model_guestBook extends Model{
    public function get_data()
    {	
        return GuestBookDB::getInstance()->Select();
    }
    
    public function insert_data($obj){
        GuestBookDB::getInstance()->InsertMessage($obj);
    }
}

class GuestBookDB extends mysqli{
    
    private static $instance = null;

    private $user = "phpuser";
    private $pass = "phpuserpw";
    private $dbName = "archive";
    private $dbHost = "localhost";
    
    private function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
        $this->query("CREATE TABLE IF NOT EXISTS messages(
            userName VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            email VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            message TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL ,
            date DATETIME NOT NULL,
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT)
            DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function DateTimeForSQL($date) {
        return $date->format("Y-m-d H:i:s");
    }
    
    public function InsertMessage($obj) {
        $this->query("INSERT INTO messages (userName, email, message, date)" .
                 " VALUES ('" . $obj['Name'] . "', '" . $obj['Email'] . "', '" . 
                $obj['Msg']. "', '" . $this->DateTimeForSQL(new DateTime()) . "')");
    }
    
    public function Select(){
        $array = array();
        $result = $this->query("SELECT * FROM messages ORDER BY date DESC");
        if($result){
            while($row = $result->fetch_array()){
                $array[] = ['Name' => $row['userName'], 'Email' => $row['email'], 'Msg' => $row['message'], 'Date' => new DateTime($row['date']), 'Number' => $row['id']];
            }
        }
        return $array;
    }
}