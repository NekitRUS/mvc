<?php
class Model_auth extends Model{
    
    public function get_user($obj){	
        return UsersDB::getInstance()->GetUser($obj);
    }
    
    public function get_verify($obj){	
        return UsersDB::getInstance()->Exist($obj);
    }
    
    public function insert_data($obj){	
        return UsersDB::getInstance()->AddUser($obj);
    }
}

class UsersDB extends mysqli{
    
    private static $instance = null;

    private $user = "phpuser";
    private $pass = "phpuserpw";
    private $dbName = "archive";
    private $dbHost = "localhost";
    
    private function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
        $this->query("CREATE TABLE IF NOT EXISTS users(
            login VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            password VARCHAR(32) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            full_name VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT)
            DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function Encrypt($str){
        $salt = 'myProtecion';
        return md5(strrev(md5($salt . $str)));
    }
    
    public function Exist($data){
        $login = $this->real_escape_string($data['login']);  
        $result = $this->query("SELECT * FROM users WHERE login='" . $login . "'");
        if($result){
            if($result->num_rows > 0){
                return true;
            }
        }         
        return false; 
    }

        public function AddUser($data){
        $full_name = $this->real_escape_string($data['name']);
        $login = $this->real_escape_string($data['login']);
        $password = $this->Encrypt($data['password']);
        $result = $this->query("SELECT * FROM users WHERE login='" . $login . "'");
        if($result){
            if (mysqli_num_rows($result) > 0){ return false; }
        }
        return $this->query("INSERT INTO users (full_name, login, password) VALUES ('" . $full_name . "', '" . $login . "', '" . $password . "')");
    }
    
    public function GetUser($data){
        $login = $this->real_escape_string($data['login']);
        $password = $this->Encrypt($data['password']);
        $result = $this->query("SELECT login, password, full_name FROM users WHERE login='" . $login . "'");
        if($result){
            $user = $result->fetch_array();
            if(($user['login'] == $login) && ($user['password'] == $password)){
                return ['login' => $user['login'], 'full_name' => $user['full_name']];
            }
        }         
        return false;
    }
}
